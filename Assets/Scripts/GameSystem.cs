using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Advertisements;

public class GameSystem : MonoBehaviour, IUnityAdsInitializationListener, IUnityAdsLoadListener, IUnityAdsShowListener
{
    [SerializeField] string _androidGameId = "4281345";
    [SerializeField] string _iOsGameId = "4281344";
    [SerializeField] bool _testMode = false;
    [SerializeField] bool _enablePerPlacementMode = true;
    private string _gameId;
    [SerializeField] string _androidAdUnitId = "Interstitial_Android";
    [SerializeField] string _androidBannerUnitId = "Banner_Android";
    [SerializeField] string _iOsAdUnitId = "Interstitial_iOS";
    [SerializeField] string _iOsBannerUnitId = "Banner_iOS";
    string _adUnitId;
    string _bannerUnitId;

    float adBannerThreshold = 0.8f;
    float adUnitProbability = 0.7f;
    int adUnitCdMax = 7;
    int adUnitCdMin = 3;
    public int adUnitCd = 0;

    public static GameSystem instance;
    public PlayerController player;
    public UIController fuelUi;
    public UIController scoreUi;
    public UIController finishUi;
    public FuelController fuelObject;
    public ScoreController scoreObject;
    public TutorialController tutorialObject;
    public FinishController finishObject;

    public Button musicButton;
    public bool isMusicEnabled = true;
    public bool isMusicActive = false;

    public bool isBannerLoaded = false;
    public bool isPaused = true;
    public bool isFinished = false;
    public float isFinishedTimer = 0.5f;

    private float score = 0.0f;

    int spawnInterval = 10;
    private GUIStyle fuelStyle = null;
    private Color fuelColor;

    float fuelTimer = 0;
    float fuelTimerMultiplier = 4;
    float scoreTimer = 0;
    float scoreTimerMultiplier = 3;
    // Start is called before the first frame update
    void Awake()
    {
        InitializeAds();

        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this) {
            Destroy(gameObject);
        }

        musicButton.onClick.AddListener(TaskOnClick);
    }
    void TaskOnClick()
    {
        isMusicEnabled = isMusicEnabled ? false : true;

        if (isMusicActive)
        {
            if (isMusicEnabled)
            {
                this.GetComponent<AudioSource>().Play();
            }
            else
            {
                this.GetComponent<AudioSource>().Pause();
            }
        }

        SaveSystem.instance.isMusicEnabled = isMusicEnabled;
        SaveSystem.instance.Save();

        musicButton.GetComponent<Image>().color = isMusicEnabled ? Color.white : Color.red;
        musicButton.GetComponentInChildren<Text>().text = "mus\n"+ (isMusicEnabled ? "on" : "off");
    }

    public void InitializeAds()
    {
        _adUnitId = (Application.platform == RuntimePlatform.IPhonePlayer)
            ? _iOsAdUnitId
            : _androidAdUnitId;

        _bannerUnitId = (Application.platform == RuntimePlatform.IPhonePlayer)
            ? _iOsBannerUnitId
            : _androidBannerUnitId;

        _gameId = (Application.platform == RuntimePlatform.IPhonePlayer)
            ? _iOsGameId
            : _androidGameId;
        Advertisement.Initialize(_gameId, _testMode, _enablePerPlacementMode, this);
    }
    public void OnInitializationComplete()
    {
        Debug.Log("Unity Ads initialization complete.");

        BannerLoadOptions bannerOptions = new BannerLoadOptions
        {
            loadCallback = OnBannerLoaded,
            errorCallback = OnBannerError
        };

        Advertisement.Banner.Load(_bannerUnitId, bannerOptions);
        Advertisement.Banner.SetPosition(BannerPosition.TOP_LEFT);
    }
    public void OnInitializationFailed(UnityAdsInitializationError error, string message)
    {
        Debug.Log($"Unity Ads Initialization Failed: {error.ToString()} - {message}");
    }
    // Load content to the Ad Unit:
    public void LoadAd()
    {
        // IMPORTANT! Only load content AFTER initialization (in this example, initialization is handled in a different script).
        //Debug.Log("Loading Ad: " + _adUnitId);
        Advertisement.Load(_adUnitId, this);
    }
    // Show the loaded content in the Ad Unit: 
    public void ShowAd()
    {
        // Note that if the ad content wasn't previously loaded, this method will fail
        //Debug.Log("Showing Ad: " + _adUnitId);
        Advertisement.Show(_adUnitId, this);
    }
    // Implement Load Listener and Show Listener interface methods:  
    public void OnUnityAdsAdLoaded(string adUnitId)
    {
        // Optionally execute code if the Ad Unit successfully loads content.
    }
    public void OnUnityAdsFailedToLoad(string adUnitId, UnityAdsLoadError error, string message)
    {
        Debug.Log($"Error loading Ad Unit: {adUnitId} - {error.ToString()} - {message}");
        // Optionally execite code if the Ad Unit fails to load, such as attempting to try again.
    }
    public void OnUnityAdsShowFailure(string adUnitId, UnityAdsShowError error, string message)
    {
        Debug.Log($"Error showing Ad Unit {adUnitId}: {error.ToString()} - {message}");
        // Optionally execite code if the Ad Unit fails to show, such as loading another ad.
    }
    public void OnUnityAdsShowStart(string adUnitId) { }
    public void OnUnityAdsShowClick(string adUnitId) { }
    public void OnUnityAdsShowComplete(string adUnitId, UnityAdsShowCompletionState showCompletionState) { }

    void OnBannerLoaded()
    {
        Debug.Log("Banner loaded");
    }
    void OnBannerError(string message)
    {
        Debug.Log($"Banner Error: {message}");
    }
    void OnBannerClicked() { }
    void OnBannerShown() { }
    void OnBannerHidden() { }
    void ShowBannerAd()
    {
        // Set up options to notify the SDK of show events:
        BannerOptions options = new BannerOptions
        {
            clickCallback = OnBannerClicked,
            hideCallback = OnBannerHidden,
            showCallback = OnBannerShown
        };

        // Show the loaded Banner Ad Unit:
        Advertisement.Banner.Show(_bannerUnitId, options);
    }
    void HideBannerAd()
    {
        // Hide the banner:
        Advertisement.Banner.Hide();
    }

    private void Start()
    {
        SaveSystem.instance.Load();
        adUnitCd = SaveSystem.instance.adUnitCd;

        isMusicEnabled = SaveSystem.instance.isMusicEnabled;
        musicButton.GetComponent<Image>().color = isMusicEnabled ? Color.white : Color.red;
    }

    public void updateFuel(float number)
    {
        fuelUi.GetComponent<TextMeshProUGUI>().text = "Fuel: " + (int)number + "%";
    }

    public void updateScore(float number)
    {
        score += number;
        scoreUi.GetComponent<TextMeshProUGUI>().text = "Score: " + (int)score + "\nTop Score: "+(int)SaveSystem.instance.scoreTop;
    }

    //[Command]
    void SpawnFuel()
    {
        var spawnPoint = Camera.main.ViewportToWorldPoint(new Vector3(Random.Range(0.1f, 0.9f), 1.1f, 5));

        Instantiate(fuelObject, spawnPoint, Quaternion.identity);
    }

    //[Command]
    void SpawnScore()
    {
        var spawnPoint = Camera.main.ViewportToWorldPoint(new Vector3(Random.Range(0.1f, 0.9f), 0.2f, 4));

        Instantiate(scoreObject, spawnPoint, Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        if (player.fuel < (player.fuelMax * adBannerThreshold) && !isPaused && !isBannerLoaded) {
            ShowBannerAd();
            isBannerLoaded = true;
        }

        if (isFinished && isFinishedTimer > 0) 
        {
            isFinishedTimer -= Time.deltaTime;
        }
        if (isFinishedTimer < 0) {
            isFinishedTimer = 0;
            finishUi.GetComponent<TextMeshProUGUI>().text = "\tScore: " + (int)score + "\n\tTop Score: " + (int)SaveSystem.instance.scoreTop;
            finishUi.GetComponent<TextMeshProUGUI>().text += "\nTap anywhere to restart";
            //finishUi.GetComponent<TextMeshProUGUI>().text += "\nPress any control button to restart";

            //������������ �� ���� ��� � 3(?) ����, �� ���� ��� � 8(?)
            //Debug.Log("adUnitCd" + adUnitCd + " vs prob"+ adUnitProbability + " : min"+ adUnitCdMin + " max" + adUnitCdMax);
            if (((Random.value > adUnitProbability) && (adUnitCd >= adUnitCdMin)) || (adUnitCd >= adUnitCdMax))
            {
                adUnitCd = 0;
                SaveSystem.instance.adUnitCd = adUnitCd;
                SaveSystem.instance.Save();
                LoadAd();
                ShowAd();
            }
        }
                
        if (!player.isUncontrollable && !isPaused)
        {
            fuelTimer += fuelTimerMultiplier * Time.deltaTime;
            scoreTimer += scoreTimerMultiplier * Time.deltaTime;
        }
        else
        {
            if ((Input.touchCount > 0) || (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A)))
            {
                if (!isFinished)
                {
                    SaveSystem.instance.Load();
                    isPaused = false;
                    player.isUncontrollable = false;
                    tutorialObject.GetComponent<Canvas>().enabled = false;
                    isMusicActive = true;
                    if (isMusicEnabled)
                    {
                        this.GetComponent<AudioSource>().Play();
                    }
                }
                else {
                    if (isFinishedTimer <= 0) SceneManager.LoadScene("sampleScene");
                }
            }
        }

        if (fuelTimer > spawnInterval)
        {
            fuelTimer = 0;
            SpawnFuel();
        }

        if (scoreTimer > spawnInterval)
        {
            scoreTimer = 0;
            SpawnScore();
        }
    }

    public void Finish()
    {
        isMusicActive = false;
        this.GetComponent<AudioSource>().Stop();
        isPaused = true;
        isFinished = true;
        isFinishedTimer = 0.5f;
        HideBannerAd();
        isBannerLoaded = false;

        if (score > SaveSystem.instance.scoreTop)
        {
            SaveSystem.instance.scoreTop = score;
        }

        adUnitCd++;
        SaveSystem.instance.adUnitCd = adUnitCd;
        SaveSystem.instance.Save();

        finishUi.GetComponent<TextMeshProUGUI>().text = "\tScore: " + (int)score + "\n\tTop Score: " + (int)SaveSystem.instance.scoreTop;
        finishObject.GetComponent<Canvas>().enabled = true;
    }

    private Texture2D MakeFuelTexture(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];
        for (int i = 0; i < pix.Length; ++i)
        {
            pix[i] = col;
        }
        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();
        return result;
    }

    public void OnGUI()
    {
        if (!player.isUncontrollable && !isPaused)
        {
            if (player.fuel > (player.fuelMax / 2))
            {
                fuelColor = Color.green;
            }
            else if (player.fuel > (player.fuelMax / 4))
            {
                fuelColor = Color.yellow;
            }
            else
            {
                fuelColor = Color.red;
            }
            if (fuelStyle == null)
            {
                fuelStyle = new GUIStyle(GUI.skin.box);
            }

            fuelStyle.normal.background = MakeFuelTexture(2, 2, fuelColor);
            GUI.Box(new Rect(0, Screen.height - Screen.height * 0.03f, Screen.width * (player.fuel / player.fuelMax), Screen.height * 0.03f), "", fuelStyle);
        }
    }
}
